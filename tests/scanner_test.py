import unittest
import os
from compiler_exceptions import LexicalErrorException
from scanner_module import Scanner

# token format
# LexToken(
#   type,   # type
#   value,  # hodnot
#   lineno, # cislo radku
#   lexpos  # cislo pozice na radku
# )


class ScannerTestCase(unittest.TestCase):

    def setUp(self):
        self.lexer = Scanner(debug=0)

    def testCrLf(self):
        lex = self.lexer
        lex.input('char\nchar')
        self.assertTrue(str(self.token()).startswith("LexToken(CHAR,'char'"))
        self.assertTrue(str(self.token()).startswith("LexToken(CHAR,'char'"))
        lex.input('char\r\nchar')
        self.assertTrue(str(self.token()).startswith("LexToken(CHAR,'char'"))
        self.assertTrue(str(self.token()).startswith("LexToken(CHAR,'char'"))

    def test_reserved(self):
        lex = self.lexer

        lex.input('break')
        self.assertEqual(str(lex.token()), "LexToken(BREAK,'break',1,0)")
        ##print 'break'

        lex.input('BREAK')
        self.assertNotEqual(str(lex.token()), "LexToken(BREAK,'break',1,0)")
        ##print 'not break'

        lex.input('char')
        self.assertEqual(str(lex.token()), "LexToken(CHAR,'char',1,0)")
        ##print 'char'

        lex.input('continue'),
        self.assertEqual(str(lex.token()), "LexToken(CONTINUE,'continue',1,0)")
        ##print 'continue'

        lex.input('else'),
        self.assertEqual(str(lex.token()), "LexToken(ELSE,'else',1,0)")
        ##print 'else'

        lex.input('for'),
        self.assertEqual(str(lex.token()), "LexToken(FOR,'for',1,0)")
        ##print 'for'

        lex.input('if'),
        self.assertEqual(str(lex.token()), "LexToken(IF,'if',1,0)")
        ##print 'if'

        lex.input('int'),
        self.assertEqual(str(lex.token()), "LexToken(INT,'int',1,0)")
        ##print 'int'

        lex.input('short'),
        self.assertEqual(str(lex.token()), "LexToken(SHORT,'short',1,0)")
        ##print 'short'

        lex.input('string'),
        self.assertEqual(str(lex.token()), "LexToken(STRING,'string',1,0)")
        ##print 'string'

        lex.input('unsigned'),
        self.assertEqual(str(lex.token()), "LexToken(UNSIGNED,'unsigned',1,0)")
        ##print 'unsigned'

        lex.input('void'),
        self.assertEqual(str(lex.token()), "LexToken(VOID,'void',1,0)")
        ##print 'void'

        lex.input('while'),
        self.assertEqual(str(lex.token()), "LexToken(WHILE,'while',1,0)")
        ##print 'while'

    def test_data_types(self):
        lex = self.lexer

        lex.input('123')
        self.assertEqual(str(lex.token()), "LexToken(INT_CONST,123,1,0)")
        #print 'data - int'

        lex.input('+123')
        self.assertNotEqual(str(lex.token()), "LexToken(INT_CONST,123,1,0)")
        #print 'data - not int'

        lex.input('-123')
        self.assertNotEqual(str(lex.token()), "LexToken(INT_CONST,123,1,0)")
        #print 'data - not int'

        lex.input("\"abc\"")
        self.assertEqual(str(lex.token()), "LexToken(STR_CONST,'abc',1,0)")
        #print "data - string"

        lex.input("abc")
        self.assertNotEqual(str(lex.token()), "LexToken(STR_CONST,'abc',1,0)")
        #print "data - not string"

        lex.input("abc")
        self.assertNotEqual(str(lex.token()), "LexToken(STR_CONST,'abc',1,0)")
        #print "data - not string"

        lex.input("'a'")
        self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'a',1,0)")
        #print "data - char"

        lex.input("\"a\"")
        self.assertNotEqual(str(lex.token()), "LexToken(CHAR_CONST,'a',1,0)")
        #print "data - not char"

        lex.input("a")
        self.assertNotEqual(str(lex.token()), "LexToken(CHAR_CONST,'a',1,0)")
        #print "data - not char"

        lex.input("'\\n'")
        self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'\\n',1,0)")
        lex.input("'\\t'")
        self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'\\t',1,0)")
        lex.input("'\\\\'")
        self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'\\\\',1,0)")
        lex.input("'\\\"'")
        self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'\"',1,0)")
        # lex.input("'\\\''")
        # self.assertEqual(str(lex.token()), "LexToken(CHAR_CONST,'\'',1,0)") # nejak to nejde, bud jsem to escapuje nebo ne

    def test_id(self):
        lex = self.lexer

        lex.input("id")
        self.assertEqual(str(lex.token()), "LexToken(ID,'id',1,0)")
        #print "id"

        lex.input("id_123")
        self.assertEqual(str(lex.token()), "LexToken(ID,'id_123',1,0)")
        #print "id"

        lex.input("_id_")
        self.assertEqual(str(lex.token()), "LexToken(ID,'_id_',1,0)")
        #print "id"

        lex.input("1id_")
        self.assertNotEqual(str(lex.token()), "LexToken(ID,'1id_',1,0)")
        #print "not id"

    def test_operators(self):
        lex = self.lexer

        lex.input("( ) ! * / % + - < <= > >= == != && ||")

        self.assertEqual(str(lex.token()), "LexToken(LPAREN,'(',1,0)")
        self.assertEqual(str(lex.token()), "LexToken(RPAREN,')',1,2)")
        self.assertEqual(str(lex.token()), "LexToken(NOT,'!',1,4)")
        self.assertEqual(str(lex.token()), "LexToken(TIMES,'*',1,6)")
        self.assertEqual(str(lex.token()), "LexToken(DIVIDE,'/',1,8)")
        self.assertEqual(str(lex.token()), "LexToken(MOD,'%',1,10)")
        self.assertEqual(str(lex.token()), "LexToken(PLUS,'+',1,12)")
        self.assertEqual(str(lex.token()), "LexToken(MINUS,'-',1,14)")
        self.assertEqual(str(lex.token()), "LexToken(LT,'<',1,16)")
        self.assertEqual(str(lex.token()), "LexToken(LE,'<=',1,18)")
        self.assertEqual(str(lex.token()), "LexToken(GT,'>',1,21)")
        self.assertEqual(str(lex.token()), "LexToken(GE,'>=',1,23)")
        self.assertEqual(str(lex.token()), "LexToken(EQ,'==',1,26)")
        self.assertEqual(str(lex.token()), "LexToken(NE,'!=',1,29)")
        self.assertEqual(str(lex.token()), "LexToken(AND,'&&',1,32)")
        self.assertEqual(str(lex.token()), "LexToken(OR,'||',1,35)")

    def test_other(self):
        lex = self.lexer

        lex.input("{ } , ;")

        self.assertEqual(str(lex.token()), "LexToken(LBRACE,'{',1,0)")
        self.assertEqual(str(lex.token()), "LexToken(RBRACE,'}',1,2)")
        self.assertEqual(str(lex.token()), "LexToken(COMMA,',',1,4)")
        self.assertEqual(str(lex.token()), "LexToken(SEMI,';',1,6)")

    def test_program(self):
        with open(os.path.dirname(__file__) + "/resources/1-iterative-factorial.c", "r") as file:
            data = file.read()

        self.lexer.input(data)

        # int main(void) {
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'main'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(VOID"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(LBRACE"))

        # int a, vysl;
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(COMMA"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'vysl'"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # #print("Zadejte cislo pro vypocet faktorialu");
        self.assertTrue(str(self.token()).startswith("LexToken(ID"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(STR_CONST,'Zadejte cislo pro vypocet faktorialu'"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # a = read_int();
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(ASSIGN"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'read_int'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # if (a < 0) {
        self.assertTrue(str(self.token()).startswith("LexToken(IF"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LT"))
        self.assertTrue(str(self.token()).startswith("LexToken(INT_CONST,0"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(LBRACE"))

        # print("\nFaktorial nelze spocitat\n"); }
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'print'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(STR_CONST,'\\\\nFaktorial nelze spocitat\\\\n'"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))
        self.assertTrue(str(self.token()).startswith("LexToken(RBRACE"))

        # else
        self.assertTrue(str(self.token()).startswith("LexToken(ELSE"))

        # {
        self.assertTrue(str(self.token()).startswith("LexToken(LBRACE"))

        # vysl = 1;
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'vysl'"))
        self.assertTrue(str(self.token()).startswith("LexToken(ASSIGN"))
        self.assertTrue(str(self.token()).startswith("LexToken(INT_CONST,1"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # while (a > 0)
        self.assertTrue(str(self.token()).startswith("LexToken(WHILE"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(GT"))
        self.assertTrue(str(self.token()).startswith("LexToken(INT_CONST,0"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))

        # {
        self.assertTrue(str(self.token()).startswith("LexToken(LBRACE"))

        # vysl = vysl * a;
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'vysl'"))
        self.assertTrue(str(self.token()).startswith("LexToken(ASSIGN"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'vysl'"))
        self.assertTrue(str(self.token()).startswith("LexToken(TIMES"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # a = a - 1;
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(ASSIGN"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'a'"))
        self.assertTrue(str(self.token()).startswith("LexToken(MINUS"))
        self.assertTrue(str(self.token()).startswith("LexToken(INT_CONST"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # }
        self.assertTrue(str(self.token()).startswith("LexToken(RBRACE"))

        # print("\nVysledek je: ", vysl, "\n");
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'print'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(STR_CONST,'\\\\nVysledek je: "))
        self.assertTrue(str(self.token()).startswith("LexToken(COMMA"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'vysl'"))
        self.assertTrue(str(self.token()).startswith("LexToken(COMMA"))
        self.assertTrue(str(self.token()).startswith("LexToken(STR_CONST,'\\\\n"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(SEMI"))

        # }
        self.assertTrue(str(self.token()).startswith("LexToken(RBRACE"))

        # }
        self.assertTrue(str(self.token()).startswith("LexToken(RBRACE"))

    def test_comments(self):
        self.lexer.input('  // comment\n'
                         'int')
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))

        self.lexer.input('int'
                         '  // comment\n')
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(self.token() == None)

        self.lexer.input('/* comment */\nint')
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))

        self.lexer.input('int'
                         '/* comment*/')
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(self.token() == None)

        self.lexer.input('int'
                         '/* comment\n'
                         '   comment\n'
                         '*/')
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(self.token() == None)


    def test_strs(self):
        self.lexer.input('int main(void) {\n'
                         '// retezec - dve uvozovky na konci\n'
                         'string promenna = "ahoj"";'
                         '}')
        # int main(void) {
        self.assertTrue(str(self.token()).startswith("LexToken(INT"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID,'main'"))
        self.assertTrue(str(self.token()).startswith("LexToken(LPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(VOID"))
        self.assertTrue(str(self.token()).startswith("LexToken(RPAREN"))
        self.assertTrue(str(self.token()).startswith("LexToken(LBRACE"))
        # string promenna = "ahoj"";
        self.assertTrue(str(self.token()).startswith("LexToken(STRING"))
        self.assertTrue(str(self.token()).startswith("LexToken(ID"))
        self.assertTrue(str(self.token()).startswith("LexToken(ASSIGN"))
        self.assertTrue(str(self.token()).startswith("LexToken(STR_CONST"))
        with (self.assertRaises(LexicalErrorException)):
            self.assertTrue(self.token())


    def token(self):
        t = self.lexer.token()
        #print t
        return t

if __name__ == '__main__':
    unittest.main()
