int main(void)
{
  char a, b, n;
  int r;
  a = 'a';
  b = 'b';
  n = '\n';
  print(a);
  print(b);
  print(n);
  
  r = a < b;
  print(r);
  r = a >= b;
  print(r);
  r = a == 'a';
  print(r);
  r = b != 'b';
  print(r);
  r = a < n;
  print(r);
  r = a > n;
  print(r);
}
