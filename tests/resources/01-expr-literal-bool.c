int main(void)
{
  int a;
  int b;
  int c;
  a = 5;
  b = 10;
  c = 1 < a;
  print(c);
  c = a > 10;
  print(c);
  c = a <= b;
  print(c);
  c = b <= a;
  print(c);
  c = 7 >= a;
  print(c);
  c = a >= 7;
  print(c);
  c = a != b;
  print(c);
  c = 10 != b;
  print(c);
  c = b == b;
  print(c);
  c = b == 5;
  print(c);
  c = a != b == b != a;
  print(c);
}
