int factorial (int n) {
  int temp_result;
  int decremented_n;
  int result;

  if (n < 2) {
    result = 1;
  }
  else {
    decremented_n = n - 1;
    temp_result = factorial(decremented_n);
    result = n * temp_result;
  }
  return result;
}

int main (void) {
  int a;
  int vysl;

  a = read_int();
  if (a < 0) {
    print("WTF");
  }
  else {
    vysl = factorial(a);
    print(vysl);
  }
}