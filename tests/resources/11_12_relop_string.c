int main(void) {

    int Kocka;
    string Tereza;
    string Alice;

    Tereza = "Tereza";
    Alice = "Alice";
        
    Kocka = "Tereza" < "Alice";
    if(  Kocka ) {
      print(1); }
    else {
      print(0);
    }
    
    Kocka = "Tereza" <= "Alice";
    if(  Kocka ) {
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" > "Alice";
    if(  Kocka ) {
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" >= "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }
	
	print('\n');

    Kocka = "Tereza" == "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" != "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice < "Alice";
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }
    
    Kocka = Alice <= "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }
	
	print('\n');

    Kocka = Alice > "Alice";
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice >= "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice == "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice != "Alice";
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

	print('\n');
	
    Kocka = "Tereza" < Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }
    
    Kocka = "Tereza" <= Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" > Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" >= Tereza;
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }
	
	print('\n');

    Kocka = "Tereza" == Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = "Tereza" != Tereza;
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice < Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }
    
    Kocka = Alice <= Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

	print('\n');
	
    Kocka = Alice > Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice >= Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice == Tereza;
    if(  Kocka) {
      print(1); }
    else {
      print(0);
    }

    Kocka = Alice != Tereza;
    if(  Kocka ){
      print(1); }
    else {
      print(0);
    }
}
