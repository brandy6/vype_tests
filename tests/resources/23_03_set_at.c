int main(void) {
  string s;
  char c;
  s = "ABCDEF";
  
  print(set_at(s, 0, 'a'));
  print('\n');
  print(set_at(s, 1, 'b'));
  print('\n');
  print(set_at(s, 2, 'c'));
  print('\n');
  print(set_at(s, 3, 'd'));
  print('\n');
  print(set_at(s, 4, 'e'));
  print('\n');
  print(set_at(s, 5, 'f'));
}