import unittest
import os
from os import listdir

from parser_module import Parser
from code_generator import CodeGenerator


# from compiler_exceptions import SyntaxErrorException, SemanticErrorException

class TestCodeGenrationTestCase(unittest.TestCase):
    def setUp(self):
        self.parser = Parser(debug=1)
        self.code_generator = CodeGenerator()

    def test_empty_main(self):
        self.generate("int main(void) {}")

        self.assertIn('.text', self.result)
        self.assertIn('jr $RA', self.result)

    ## EMBEDED FUNCTIONS
    ############################################
    # print

    def test_declaration_default_value(self):
        self.generate('int main(void) { int a; return a; }')

        # print "\nOUTPUT asembler:"
        # print self.result

        self.assertIn('li', self.result)
        self.assertIn('push', self.result)

    # read_char
    def test_input(self):
        self.generate('int main(void) { read_char(); read_int(); read_string(); }')

        self.assertIn('read_char $', self.result)
        self.assertIn('read_int $', self.result)
        self.assertIn('read_string $', self.result)

    def test_assign(self):
        self.generate('void f(void) {} int main(void) { int a; a = 1; print(a);}')

        self.assertIn('addi $sp, $sp, 4', self.result)


    def test_work(self):
        self.generate("int main(void) {while(1) {}}")

    ###############################################
    # METHODS - UNIT TESTS

    def test_function_def_label(self):
        result_arr = self.code_generator.function('func_name', 1, 0)

        self.assertIn('func_label_func_name:', result_arr)

    def test_function_save_base_locals(self):
        result_arr = self.code_generator.function('func_name', 2, 0)

        self.assertIn('move $25, $sp', self.joined_instructions(result_arr))
        self.assertEqual(3, self.code_generator.reserved_locals_count)

    def test_load_int(self):
        result_arr = self.code_generator.load_int(2, 'x1', 'x2')

        self.assertIn('li', self.joined_instructions(result_arr))
        self.assertIn('push', self.joined_instructions(result_arr))


    def test_allocate_string(self):
        result = self.code_generator.allocate_string('012')
        result = self.joined_instructions(result)

        self.assertIn('push', result)

        # 0
        self.assertIn('li', result)
        self.assertIn('48', result)
        self.assertIn('sb', result)
        self.assertIn('0($23)', result)

        # 1
        self.assertIn('49', result)
        self.assertIn('1($23)', result)

        # 2
        self.assertIn('50', result)
        self.assertIn('2($23)', result)

        # '\0'
        self.assertIn('3($23)', result)

    ###############################################
    # helpers
    def joined_instructions(self, instructions):
        return "\n".join(
          instructions
        )

    def generate(self, input_program):
        self.instructions = self.parser.parse(input_program)
        self.result = self.code_generator.generate(self.instructions)

    def test_source(self):
        dir = os.path.dirname(__file__) + '/resources'
        sources = [f for f in listdir(dir) if os.path.splitext(f)[1] == '.c']
        for source in sources:
            if source == "21_01_read_char.c":
                with open(dir + '/' + source, "r") as f:
                    data = f.read()
                    self.generate(data)
                    pass

if __name__ == '__main__':
    unittest.main(verbosity=2)
