import unittest
import os

from parser_module import Parser
from compiler_exceptions import SyntaxErrorException, SemanticErrorException


class TestSemanticTestCase(unittest.TestCase):

    def test_syntax_error(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse("void m ain() {};")

    def test_empty_input(self):
        with self.assertRaises(SemanticErrorException):
            self.parse("")

    # function definition
    # ##############################################
    def test_func_definition(self):
        self.parse("int main(void) {}")

    def test_func_multiple_definitions(self):
        self.parse("int main(void) {} void myFunc1(void) {} void myFunc2(void) {}")

    # function declaration
    ###############################################
    def test_func_declaration(self):
        self.parse("int main(void) {} void myFunc(void) {}")

    def test_func_multiple_declarations(self):
        self.parse("int main(void) {} void myFunc1(void) {} void myFunc2(void) {}")

    def test_func_multiple_definitions2(self):
        self.parse("int main(void) {} void myFunc1(void){} void myFunc2(void){}")

    # paramaters
    ###############################################
    def test_invalid_data_parameter(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse('char f(int p1, void p2)')

    def test_invalid_parameters(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse('char f(int p1, void)')

    def test_void_is_not_in_params(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse("void myFunc(void, int, string) {}")
            self.parse("void myFunc(string, void, string) {}")
            self.parse("void myFunc(char, int, int, void) {}")

    # data paramaters
    ###############################################
    def test_data_params(self):
        with self.assertRaises(SemanticErrorException):
            self.parse("void myFunc(int);")
        with self.assertRaises(SemanticErrorException):
            self.parse("void myFunc(char, int);")
        with self.assertRaises(SemanticErrorException):
            self.parse("void myFunc(char, int, string);")

    def test_void_is_not_in_data_params(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse("void myFunc(void, int, string);")
            self.parse("void myFunc(string, void, string);")
            self.parse("void myFunc(char, int, int, void);")

    # declaration of variables
    ###############################################
    def test_declaration(self):
        self.parse('int main(void){ int a; }')
        self.parse('int main(void){ char a, b; }')
        self.parse('int main(void){ string a, b, c; }')

    def test_void_is_not_data_type(self):
        with self.assertRaises(SyntaxErrorException):
            self.parse('int main(void){ void a; }')

    # # assignment
    # ###############################################
    def test_assignment(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ a = b; }')
        self.parse('int main(void){ int a, b; a = b; }')

    # if-else
    ###############################################
    def test_if_else(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ if (a) { } else {}  }')

        self.parse('int main(void){int a, b, c; if (a + b > c) { } else {}  }')

        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){int a, b, c; if (a + b > c) { d = 1; } else {}  }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){int a, b, c; if (a + b > c) { a = d + 1; } else {}  }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){int a, b, c; if (a + b > c) { } else { d = 1; }  }')

        self.parse('int main(void){int a, b, c; if (a + b > c) { int d; d = 1; } else { }  }')
        self.parse('int main(void){int a, b, c; if (a + b > c) { } else { int d; d = 1; }  }')

        self.parse('int main(void){int a, b, c, d; if (a + b > c) { } else { d = 1; }  }')
        self.parse('int main(void){int a, b, c, d; if (a + b > c) { d = 1; } else { }  }')

    # while
    ###############################################
    def test_while(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ while (a) { } }')

        self.parse('int main(void){ int a; while (a > 1) { int b; b = a; } }')

    # function call
    ###############################################
    def test_function_call(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ fun(); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ fun(a); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ fun(a, b); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ fun(a, b, c); }')

        self.parse('void fun(void) {} int main(void){ fun(); }')

        with self.assertRaises(SemanticErrorException):
            self.parse('void fun(void); int main(void){ int a; a = fun(); }')

        with self.assertRaises(SemanticErrorException):
            self.parse('int fun(void); int main(void){int a,b,c; fun(a, b, c); }')

        with self.assertRaises(SemanticErrorException):
            self.parse('int fun(string, int,int); int main(void){int a,b,c; fun(a, b, c); }')

    # return
    ###############################################
    def test_return(self):
        with self.assertRaises(SemanticErrorException):  # return void
            self.parse('int main(void){ return ; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ return a; }')

    def test_return_not_expression_list(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void) { return a, b; }')

    # expressions - in order of ascending priority (lowest -> highest)
    ###############################################
    def test_logical_expressions(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a || b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a && b; }')

    def test_relational_expressions(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a < b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a > b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a <= b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a >= b; }')

    def test_equality_expressions(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a == b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a != b; }')

    def test_arithmetic_expressions(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a + b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a - b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a % b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a * b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a / b; }')

    def test_not_expression(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = !b; }')

    def test_casting_expression__(self):
        self.parse('int main(void) { string str1; int a; a = (int)((((get_at(str1, 1))))) != 0;}')

    def test_casting_expression(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = (int)b; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = (char)b; }')

        self.parse('int main(void) { string str1; int a; a = (int)((((get_at(str1, 1))))) != 0;}')
        self.parse('int main(void) { string str1; int a; a = (((int)((((get_at(str1, 1))))))) != 0;}')
        self.parse('int main(void) { string str1; int a; a = (int)(get_at(str1, 1)) != 0;}')
        self.parse('int main(void) { string str1; int a; a = (int)get_at(str1, 1) != 0;}')

        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void) { string str1; int a; int x; a = get_at(str1, 1) != 0;}')
        self.parse('int main(void) { string str1; int a; a = get_at(str1, 1) != \'a\';}')
        self.parse('int main(void) { string str1; int a; a = a - 1; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void) { string str1; int a; a = a - \'a\'; }')
        self.parse('int main(void) { int a; a = ((1 != 1) - 1) > 0 && 1; }')

    def test_function_call_expression(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = b(); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int fun(int, string, int); int main(void){int a, b, c; fun(a * 5, b + 2, c % (5 / 2)); }')

    def test_parantheses(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = (a + b); }')

    def test_compound_expressions(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a / b + d; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a * b + d; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a * b + c; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ r = a / b - c; }')

    def test_expressions_inside_statements(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ if (a + b > c) { } else {}  }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ while (a >= b) { } }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ fun(a * 5, b + "c", c % (5 / 2)); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ return (int)(a * 8 * 5 + 2); }')

    def test_redefinition(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void) {int a; int a;}')

    def test_variable_scope(self):
        self.parse('int main(void){ int a; a = 1; if (1) { } else {}  }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ int a; a = 1; if (1) { int b; } else {} a = a + b; }')
        self.parse('int main(void){ int a; a = 1; if (1) { int b; } else { int b; }  }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ int a; a = 1; if (1) { int b; } else { int b; } a = a + b; }')

    def test_cast(self):
        # char -> string
        self.parse('int main(void){ char a; string b; a = \'x\'; b = (string)a; }')
        # char -> int
        self.parse('int main(void){ char a; int b; a = \'x\'; b = (int)a; }')
        # int -> char
        self.parse('int main(void){ int a; char b; a = 1; b = (char)a; }')
        # T -> T
        self.parse('int main(void){ int a; int b; a = 1; b = (int)a; }')
        self.parse('int main(void){ char a; char b; a = \'x\'; b = (char)a; }')
        self.parse('int main(void){ string a; string b; a = "1"; b = (string)a; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ string a; int b; a = "1"; b = (int)a; }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(void){ int a; string b; a = 1; b = (string)a; }')

        self.parse('int main(void){ int a; int b; int c; a = 1; b = 2; c = (int)\'a\';}')
        self.parse('int main(void){ int a; a = ((int)(char)1);}')

    def test_function_definition(self):
        self.parse('void f(int a, int b, int c) {} int main(void){ f(1, 2, 3); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int);  int main(void){ f(1, 2, "a"); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int); void f(int a, int b, string c) {} int main(void){ f(1, 2, 3); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int); void f(int a) {} int main(void){ f(1, 2, 3); }')

        self.parse('void f(int, int, int); void f(int a, int b, int c) {} ' +
                   'int main(void){ f(1, 2, 3); }')

        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int); void f(int, int, int); void f(int a, int b, int c) {} ' +
                       'int main(void){ f(1, 2, 3); }')

        self.parse('void f(int, int, int); void f(int a, int b, int c) {} ' +
                   'int main(void){ f(1, 2, 3); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int); void f(int, int, int); void f(int a, int b, int c) {} ' +
                       'int main(void){ f(1, 2, 3); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int, int, int); void f(int, int, int); int main(void){ f(1, 2, 3); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('int f(int, int, int); void f(int a, int b, int c) {} int main(void){ f(1, 2, 3); }')

        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int a, int b, int c) {} void f(int a, int b, int c) {} int main(void){ f(1, 2, 3); }')

    def test_function_args(self):
        self.parse('void f(int a) {} int main(void){ f(1); }')
        self.parse('void f(int a) {a = 1;} int main(void){ f(1); }')
        self.parse('void f(int a) {int b; a = 1; if (1) {int b;} else {int c;} } int main(void){ f(1); }')
        self.parse('void f(int a) { int b; if (1) { int b; } else { int b; } } int main(void){ f(1); }')
        self.parse('void f(int a) { int b; if (1) { int b; } else { int b; } }' +
                   'void d(int a) { int b; if (1) { int b; } else { int b; } }' +
                   'int main(void){ f(1); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int a) {int a;} int main(void){ f(1); }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int a) {int b; a = 1; if (1) {int b;} else {int c;} a = c + 1;} int main(void){ f(1); }')

    def test_function_args2(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('void f(int a) {int a;} int main(void){ f(1); }')

    def test_main(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('int main(int a){ }')
        with self.assertRaises(SemanticErrorException):
            self.parse('void main(void){ }')

    def test_ret_type(self):
        self.parse('int f(void) {} int main(void){ int a; a = f();}')
        with self.assertRaises(SemanticErrorException):
            self.parse('int f(void) {} int main(void){ string a; a = f();}')

    def test_logical_type(self):
        self.parse('int main(void){ int a; a = "Tereza" < "Alice";}')

    def test_type_return_exp(self):
        with self.assertRaises(SemanticErrorException):
            self.parse('string f(void) { return 1;}'
                       'int main(void){'
                       '}')
    #
    ###############################################
    def test_(self):
        pass

    # programs
    ###############################################
    def test_iterative_factorial(self):
        with open(os.path.dirname(__file__) + "/resources/1-iterative-factorial.c", "r") as f:
            data = f.read()

        self.parse(data)

    def test_recursive_factorial(self):
        with open(os.path.dirname(__file__) + "/resources/2-recursive-factorial.c", "r") as f:
            data = f.read()

        self.parse(data)

    def test_string_and_built_in_operations(self):
        with open(os.path.dirname(__file__) + "/resources/3-string-and-built-in-operations.c", "r") as f:
            data = f.read()

        self.parse(data)

    ###############################################
    # shortcut
    def parse(self, i):
        parser = Parser(debug=1)
        return parser.parse(i)

        # def tearDown(self):


if __name__ == '__main__':
    unittest.main(verbosity=2)
