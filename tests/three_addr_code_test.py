import unittest
from data_types import *
from instructions import *

from parser_module import Parser


class TreeAddrCodeTestCase(unittest.TestCase):

    ### adresovani

    def test_simple_assign(self):
        instr = self.parse('int main(void){int a; a = 1;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

        instr = self.parse('int main(void){int a; a = 1; int b; b = 1;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr and (ASSIGN, 2, None, None))

        instr = self.parse('int main(void){int a; a = 1; int b; b = 1; int c;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr and (ASSIGN, 2, None, None))

    def test_simple_f_param_assign(self):
        instr = self.parse('int main(void){} int f(void){int a; a = 1;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

        instr = self.parse('int main(void){} int f(int a){a = 1;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

        instr = self.parse('int main(void){} int f(int a, int b, int c){c = 1;}')
        self.assertTrue((ASSIGN, 3, INT, None) in instr)

    def test_simple_f_param_locals_assign(self):
        instr = self.parse('int main(void){} int f(int a){int b; a = 1;}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

        instr = self.parse('int main(void){} int f(int a){int b; b = 1;}')
        self.assertTrue((ASSIGN, 2, INT, None) in instr)

    def test_simple_f_param_locals_assign_nested1(self):
        instr = self.parse('int main(void){}'
                           'int f(int a){'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {}}')
        self.assertTrue((ASSIGN, 3, INT, None) in instr)

    def test_simple_f_param_locals_assign_nested2(self):
        instr = self.parse('int main(void){}'
                           'int f(int a){'
                           '    int b; '
                           '    if (1) {int a; b = 1;}'
                           '    else {}}')
        self.assertTrue((ASSIGN, 2, INT, None) in instr)

    def test_simple_f_param_locals_assign_nested3(self):
        instr = self.parse('int main(void){}'
                           'int f(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int c;}'
                           '    else {}}')
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

    def test_simple_f_param_locals_assign_nested4(self):
        instr = self.parse('int main(void){}'
                           'int f(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {int a; a = 1;}}')
        self.assertTrue(instr.count((ASSIGN, 3, INT, None)) == 4)  # 2 assign 2 implicit

    def test_simple_f_param_locals_assign_nested_2times(self):
        instr = self.parse('int main(void){}'
                           'int f(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {int a; a = 1;}}'
                           'int g(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {int a; a = 1;}}')
        self.assertTrue(instr.count((ASSIGN, 3, INT, None)) == 8)  # 4 assign 4 implicit

    def test_simple_f_param_locals_assign_nested_2times_call(self):
        instr = self.parse('int f(int);'
                           'int main(void){'
                           '    f(1);'
                           '}'
                           'int f(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {int a; a = 1;}}'
                           'int g(int a){'
                           '    a = 1;'
                           '    int b; '
                           '    if (1) {int a; a = 1;}'
                           '    else {int a; a = 1;}}')
        self.assertTrue(instr.count((ASSIGN, 3, INT, None)) == 8)
        self.assertTrue((FUNCTION, 'f', 3, 1) in instr)
        self.assertTrue((FUNCTION, 'g', 3, 1) in instr)

    def test_simple_f_param_locals_assign_nested_2times_call2(self):
        instr = self.parse('void f(int);'
                           'int main(void){'
                           '    f(1);'
                           '}'
                           'void f (int x) {'
                           '  if (1) {'
                           '    int a;'
                           '  }'
                           '  else {'
                           '    int a;'
                           '  }'
                           '  if (1) {'
                           '    int a;'
                           '  }'
                           '  else {'
                           '    int a;'
                           '  }'
                           '}')
        self.assertTrue((FUNCTION, 'f', 2, 1) in instr)
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 2, 1),
            (JMP_IF_FALSE, "label_else_0", None, None),
            (LOAD_INT, 0, None, None),
            (ASSIGN, 2, INT, None),
            (LABEL, "label_else_0", None, None),
            (LOAD_INT, 0, None, None),
            (ASSIGN, 2, INT, None),
            (LABEL, "label_end_0", None, None),

            (JMP_IF_FALSE, "label_else_1", None, None),
            (LOAD_INT, 0, None, None),
            (ASSIGN, 2, INT, None),
            (LABEL, "label_else_1", None, None),
            (LOAD_INT, 0, None, None),
            (ASSIGN, 2, INT, None),
            (LABEL, "label_end_1", None, None),

        ])

    def test_simple_f_param_locals_assign_nested_2times_call4(self):
        instr = self.parse('int main(void){'
                           '    int i;'
                           '    i = 1;'
                           '    if (1) {'
                           '        int i;'
                           '    }'
                           '    else {'
                           '        string s;'
                           '        i = 1;'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 0, None, None),
            (LOAD_INT, 1, None, None),
            (ASSIGN, 1, INT, None),
            (JMP_IF_FALSE, "label_else_0", None, None),
            (LOAD_INT, 0, None, None),
            (ASSIGN, 2, INT, None),
            (LABEL, "label_else_0", None, None),
            (LOAD_STR, "", None, None),
            (ASSIGN, 2, STRING, None),
            (LOAD_INT, 1, None, None),
            (ASSIGN, 1, INT, None),
            (LABEL, "label_end_0", None, None)
        ])

    def test_simple_f_param_locals_assign_nested_2times_call3(self):
        instr = self.parse('void f(int);'
                           'int main(void){'
                           '    f(1);'
                           '}'
                           'void f (int x) {'
                           '  if (1) {'
                           '    int a;'
                           '    if (1) {int a;} else {}'
                           '  }'
                           '  else {'
                           '    int a;'
                           '  }'
                           '  if (1) {'
                           '    int a;'
                           '  }'
                           '  else {'
                           '    int a;'
                           '  }'
                           '}')
        self.assertTrue((FUNCTION, 'f', 3, 1) in instr)
    ### expr literal

    def test_expr_literals1(self):
        instr = self.parse('int main(void){} int f(string a){a = "a";}')
        self.assertTrue((LOAD_STR, "a", None, None) in instr)
        self.assertTrue((ASSIGN, 1, STRING, None) in instr)

    def test_expr_literals2(self):
        instr = self.parse('int main(void){} int f(int a){a = 1;}')
        self.assertTrue((LOAD_INT, 1, None, None) in instr)
        self.assertTrue((ASSIGN, 1, INT, None) in instr)

    def test_expr_literals3(self):
        instr = self.parse('int main(void){} int f(char a){a = \'a\';}')
        self.assertTrue((LOAD_CHAR, 97, None, None) in instr)
        self.assertTrue((ASSIGN, 1, CHAR, None) in instr)

    ### return

    def test_return_expr0(self):
        instr = self.parse('int main(void){} void f(void){return;}')
        self.assertTrue((RETURN_VOID, None, None, None) in instr)

    def test_return_expr1(self):
        instr = self.parse('int main(void){} int f(void){return 1;}')
        self.assertTrue((LOAD_INT, 1, None, None) in instr)
        self.assertTrue((RETURN, None, None, None) in instr)

    def test_return_expr2(self):
        instr = self.parse('int main(void){} char f(void){return \'a\';}')
        self.assertTrue((LOAD_CHAR, 97, None, None) in instr)
        self.assertTrue((RETURN, None, None, None) in instr)

    def test_return_expr3(self):
        instr = self.parse('int main(void){} string f(void){return "a";}')
        self.assertTrue((LOAD_STR, "a", None, None) in instr)
        self.assertTrue((RETURN, None, None, None) in instr)

    ### if-else

    def test_if_else1(self):
        instr = self.parse('int main(void){} int f(void){'
                           '    if (1) {'
                           '        return 1;'
                           '    }'
                           '    else {'
                           '        return 2;'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (JMP_IF_FALSE, "label_else_0", None, None),
            (JMP, "label_end_0", None, None),
            (LABEL, "label_else_0", None, None),
            (LABEL, "label_end_0", None, None)
        ])

    ### while

    def test_while1(self):
        instr = self.parse('int main(void){} int f(void){'
                           '    while(1) {'
                           '        return 0;'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (JMP_IF_FALSE, "label_end_0", None, None),
            (RETURN, None, None, None),
            (LABEL, "label_end_0", None, None)
        ])

    def test_while2(self):
        instr = self.parse('int main(void){} int f(void){'
                           '    while(1+1) {'
                           '        return 0;'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (LABEL, "label_while_0", None, None),
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (ADD, INT, None, None),
            (JMP_IF_FALSE, "label_end_0", None, None),
            (RETURN, None, None, None),
            (JMP, "label_while_0", None, None),
            (LABEL, "label_end_0", None, None)
        ])

    ### while + if

    def test_if_while1(self):
        instr = self.parse('int main(void){} int f(void){'
                           '    if (1) {}'
                           '    else {}'
                           '    while(1) {'
                           '        return 0;'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (JMP_IF_FALSE, "label_else_0", None, None),
            (JMP, "label_end_0", None, None),
            (LABEL, "label_else_0", None, None),
            (LABEL, "label_end_0", None, None),
            (JMP_IF_FALSE, "label_end_1", None, None),
            (RETURN, None, None, None),
            (LABEL, "label_end_1", None, None)
        ])

    ### call

    def test_call1(self):
        instr = self.parse('int f(void){ } int main(void){ f(); }')
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 0, 0),
            (CALL, "f", 0, None)
        ])

    def test_call2(self):
        instr = self.parse('int f(int a){ } int main(void){ f(5); }')
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 1, 1),
            (LOAD_INT, 5, None, None),
            (CALL, "f", 1, None)
        ])

    def test_call3(self):
        instr = self.parse('int f(int a, char c, string s){ } int main(void){ f(0, \'d\', "f"); }')
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 3, 3),
            (LOAD_INT, 0, None, None),
            (LOAD_CHAR, 100, None, None),
            (LOAD_STR, "f", None, None),
            (CALL, "f", 3, None)
        ])

    def test_call4(self):
        instr = self.parse('int f(int a, char c, string s){ '
                           '    int x;'
                           '    int y, z;'
                           '} '
                           'int main(void){ f(0, \'d\', "f"); }')
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 6, 3),
            (LOAD_INT, 0, None, None),
            (LOAD_CHAR, 100, None, None),
            (LOAD_STR, "f", None, None),
            (CALL, "f", 3, None)
        ])

    ### expr

    ### add
    def test_plus1(self):
        instr = self.parse('int main(void){ int a; a = 1 + 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_plus2(self):
        instr = self.parse('int main(void){ int a; a = a + 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_plus3(self):
        instr = self.parse('int main(void){ int a; a = a + a; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD, 1, INT, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    ### sub
    def test_sub1(self):
        instr = self.parse('int main(void){ int a; a = 1 - 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (SUB, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_sub2(self):
        instr = self.parse('int main(void){ int a; a = a - 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (SUB, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    # mul
    def test_mul1(self):
        instr = self.parse('int main(void){ int a; a = 1 * 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (MUL, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_mul2(self):
        instr = self.parse('int main(void){ int a; a = a * 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (MUL, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    # div
    def test_div1(self):
        instr = self.parse('int main(void){ int a; a = 1 / 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (DIV, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_div2(self):
        instr = self.parse('int main(void){ int a; a = a / 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (DIV, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    # modulo
    def test_mod1(self):
        instr = self.parse('int main(void){ int a; a = 1 % 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (MOD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_mod2(self):
        instr = self.parse('int main(void){ int a; a = a % 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (MOD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    ### logical
    ### OR
    def test_or1(self):
        instr = self.parse('int main(void){ int a; a = 1 || 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (OR, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_or2(self):
        instr = self.parse('int main(void){ int a; a = a || 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (OR, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### AND
    def test_and1(self):
        instr = self.parse('int main(void){ int a; a = 1 && 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (AND, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_and2(self):
        instr = self.parse('int main(void){ int a; a = a && 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (AND, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### NOT
    def test_not1(self):
        instr = self.parse('int main(void){ int a; a = !1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (NOT, None, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_not2(self):
        instr = self.parse('int main(void){ int a; a = !a; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (NOT, None, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### LT
    def test_lt1(self):
        instr = self.parse('int main(void){ int a; a = 1 < 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (LT, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_lt2(self):
        instr = self.parse('int main(void){ int a; a = a < 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (LT, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    ### GT
    def test_gt1(self):
        instr = self.parse('int main(void){ int a; a = 1 > 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (GT, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_gt2(self):
        instr = self.parse('int main(void){ int a; a = a > 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (GT, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### LE
    def test_le1(self):
        instr = self.parse('int main(void){ int a; a = 1 <= 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (LE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_le2(self):
        instr = self.parse('int main(void){ int a; a = a <= 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (LE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### GE
    def test_ge1(self):
        instr = self.parse('int main(void){ int a; a = 1 >= 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (GE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_ge2(self):
        instr = self.parse('int main(void){ int a; a = a >= 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (GE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])
    ### EQ
    def test_eq1(self):
        instr = self.parse('int main(void){ int a; a = 1 == 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (EQ, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_eq2(self):
        instr = self.parse('int main(void){ int a; a = a == 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (EQ, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_eq3(self):
        instr = self.parse('int main(void){ char a;int x; x = a == \'a\'; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, CHAR, None),
            (LOAD_CHAR, 97, None, None),
            (EQ, CHAR, None, None),
            (ASSIGN, 2, INT, None)
        ])

    def test_eq4(self):
        instr = self.parse('int main(void){ int a; string s; a = s == ""; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 2, STRING, None),
            (LOAD_STR, "", None, None),
            (EQ, STRING, None, None),
            (ASSIGN, 1, INT, None)
        ])

    ### NE
    def test_ne1(self):
        instr = self.parse('int main(void){ int a; a = 1 != 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (NE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_ne2(self):
        instr = self.parse('int main(void){ int a; a = a != 1; }')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (NE, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    ### complex expression
    def test_complex_expr1(self):
        instr = self.parse('int main(void){ int a; a = 1 + 2 + 3; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (ADD, INT, None, None),
            (LOAD_INT, 3, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr2(self):
        instr = self.parse('int main(void){ int a; a = 1 + (2 + 3); }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (ADD, INT, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr3(self):
        instr = self.parse('int main(void){ int a; a = 1 + (2 + 3) * 4; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),

            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (ADD, INT, None, None),
            (LOAD_INT, 4, None, None),
            (MUL, INT, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr4(self):
        instr = self.parse('int main(void){ int a; a = 1 + (2 + 3) * 4 > 1 + 2 + 3 * 4; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (ADD, INT, None, None),
            (LOAD_INT, 4, None, None),
            (MUL, INT, None, None),
            (ADD, INT, None, None),

            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (LOAD_INT, 4, None, None),
            (MUL, INT, None, None),
            (ADD, INT, None, None),

            (GT, INT, None, None),

            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr5(self):
        instr = self.parse('int main(void){ int a; a = 1 + (2 + 3) * 4 > 1 + 2 * 3 + 4; }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (ADD, INT, None, None),
            (LOAD_INT, 4, None, None),
            (MUL, INT, None, None),
            (ADD, INT, None, None),

            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (LOAD_INT, 3, None, None),
            (MUL, INT, None, None),
            (LOAD_INT, 4, None, None),
            (ADD, INT, None, None),

            (GT, INT, None, None),

            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr6(self):
        instr = self.parse('int main(void){ '
                           '    int a; int b; int c; int x;'
                           '    a = 1; b = 2; c = 3;'
                           '    x = a >= 2 && (b < 3 || c == 1 + 1); }')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (ASSIGN, 1, INT, None),
            (LOAD_INT, 2, None, None),
            (ASSIGN, 2, INT, None),
            (LOAD_INT, 3, None, None),
            (ASSIGN, 3, INT, None),
            # a 2
            (LOAD, 1, INT, None),
            (LOAD_INT, 2, None, None),
            (GE, INT, None, None),
            # b 3
            (LOAD, 2, INT, None),
            (LOAD_INT, 3, None, None),
            # b < 3
            (LT, INT, None, None),
            # c 1 1
            (LOAD, 3, INT, None),
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            # 1 + 1
            (ADD, INT, None, None),
            # c == X
            (EQ, INT, None, None),
            # A || B
            (OR, INT, None, None),
            # A && B
            (AND, INT, None, None),

            (ASSIGN, 4, INT, None)
        ])

    def test_return_expr(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int main(void){}')
        self.assert_contains_sequence(instr, [
            (FUNCTION, "f", 2, 2),
            (LOAD, 1, INT, None),
            (LOAD, 2, INT, None),
            (ADD, INT, None, None),
            (RETURN, None, None, None)
        ])

    def test_complex_expr_with_call1(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = f(1,1) + 1;'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 1, None, None),
            (CALL, "f", 2, None),
            (LOAD_INT, 1, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr_with_call2(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = f(a,a) + a;'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD, 1, INT, None),
            (CALL, "f", 2, None),
            (LOAD, 1, INT, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr_with_call3(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int g(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = f(a,a) + g(a,a);'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD, 1, INT, None),
            (CALL, "f", 2, None),
            (LOAD, 1, INT, None),
            (LOAD, 1, INT, None),
            (CALL, "g", 2, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_complex_expr_with_call4(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int g(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = f(g(1,2),g(3,4));'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (CALL, "g", 2, None),
            (LOAD_INT, 3, None, None),
            (LOAD_INT, 4, None, None),
            (CALL, "g", 2, None),
            (CALL, "f", 2, None),
            (ASSIGN, 1, INT, None)
        ])

    # if with expr

    def test_if_expr1(self):
        instr = self.parse('int main(void){'
                           '    int a;'
                           '    a = 10;'
                           '    if (a > 10) {} else {}'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 10, None, None),
            (GT, INT, None, None),
            (JMP_IF_FALSE, "label_else_0", None, None),
            (JMP, "label_end_0", None, None),
            (LABEL, "label_else_0", None, None),
            (LABEL, "label_end_0", None, None),
        ])

    def test_if_expr2(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = 1;'
                           '    if (f(a, 1) > 10) {} else {}'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (CALL, "f", 2, None),
            (LOAD_INT, 10, None, None),
            (GT, INT, None, None),
            (JMP_IF_FALSE, "label_else_0", None, None),
            (JMP, "label_end_0", None, None),
            (LABEL, "label_else_0", None, None),
            (LABEL, "label_end_0", None, None)
        ])

    def test_if_expr3(self):
        instr = self.parse('int f(int a, int b) { return a + b;}'
                           'int main(void){'
                           '    int a;'
                           '    a = 1;'
                           '    while (f(a, 1) > 10) {'
                           '    }'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (LOAD_INT, 1, None, None),
            (CALL, "f", 2, None),
            (LOAD_INT, 10, None, None),
            (GT, INT, None, None),
            (JMP_IF_FALSE, "label_end_0", None, None),
            (LABEL, "label_end_0", None, None)
        ])

    def test_implicit_return1(self):
        instr = self.parse('int f(void) { return 1;}'
                           'int main(void){'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (RETURN, None, None, None),
            (LOAD_INT, 0, None, None),
            (RETURN, None, None, None)
        ])

    def test_implicit_return2(self):
        instr = self.parse('char f(void) { return \'a\';}'
                           'int main(void){'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_CHAR, 97, None, None),
            (RETURN, None, None, None),
            (LOAD_CHAR, 0, None, None),
            (RETURN, None, None, None)
        ])

    def test_implicit_return3(self):
        instr = self.parse('string f(void) { return "A";}'
                           'int main(void){'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_STR, "A", None, None),
            (RETURN, None, None, None),
            (LOAD_STR, "", None, None),
            (RETURN, None, None, None)
        ])

    def test_implicit_return4(self):
        instr = self.parse('int f(void) {'
                           '    int a;'
                           '    if (0) {'
                           '        return 1;'
                           '    }'
                           '    else {'
                           '        return 2;'
                           '    }'
                           '    a = 1;'
                           '}'
                           'int main(void){'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (RETURN, None, None, None),
            (LOAD_INT, 2, None, None),
            (RETURN, None, None, None),
            (LOAD_INT, 0, None, None),
            (RETURN, None, None, None)
        ])

    def test_build_in_print(self):
        instr = self.parse('int main(void){'
                           '    print(1, "2", \'a\');'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_STR, "2", None, None),
            (LOAD_CHAR, 97, None, None),
            (PRINT_INT, 2, None, None),
            (PRINT_STRING, 1, None, None),
            (PRINT_CHAR, 0, None, None),
            (DECREASE_STACK, 3, None, None)
        ])

    def test_build_in_print2(self):
        instr = self.parse('int f(void) {}'
                           'int main(void){'
                           '    print(f(), 1+1, (int)\'a\');'
                           '}')
        self.assert_contains_sequence(instr, [
            (CALL, 'f', 0, None),
            (ADD, INT, None, None),
            (CAST_TO, INT, CHAR, None),
            (PRINT_INT, 2, None, None),
            (PRINT_INT, 1, None, None),
            (PRINT_INT, 0, None, None),
            (DECREASE_STACK, 3, None, None)
        ])

    def test_build_in_read_int(self):
        instr = self.parse('int main(void){'
                           '    int i;'
                           '    i = read_int();'
                           '}')
        self.assert_contains_sequence(instr, [
            (CALL, "read_int", 0, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_build_in_get_at(self):
        instr = self.parse('int main(void){'
                           '    string s;'
                           '    int i;'
                           '    char c;'
                           '    c = get_at(s, i);'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, STRING, None),
            (LOAD, 2, INT, None),
            (CALL, "get_at", 2, None),
            (ASSIGN, 3, CHAR, None)
        ])

    def test_build_in_set_at(self):
        instr = self.parse('int main(void){'
                           '    string s;'
                           '    int i;'
                           '    char c;'
                           '    s = set_at(s, i, c);'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, STRING, None),
            (LOAD, 2, INT, None),
            (LOAD, 3, CHAR, None),
            (CALL, "set_at", 3, None),
            (ASSIGN, 1, STRING, None)
        ])

    ### cast

    def test_cast1(self):
        instr = self.parse('int main(void){'
                           '    int i;'
                           '    i = (int)i;'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (CAST_TO, INT, INT, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_cast_2(self):
        instr = self.parse('int main(void){'
                           '    int i;'
                           '    i = (int)i + (int)i;'
                           '}')
        print instr
        self.assert_contains_sequence(instr, [
            (LOAD, 1, INT, None),
            (CAST_TO, INT, INT, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_cast2(self):
        instr = self.parse('int main(void){'
                           '    int i;'
                           '    i = (int)\'a\' + 1;'
                           '}')
        self.assert_contains_sequence(instr, [
            (LOAD_CHAR, 97, None, None),
            (CAST_TO, INT, CHAR, None),
            (LOAD_INT, 1, None, None),
            (ADD, INT, None, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_void_return(self):
        instr = self.parse('void f(void) {} int main(void) {f();}')

        self.assert_contains_sequence(instr, [
            (CALL, "f", 0, None),
            (POP, None, None, None)
        ])

    def test_return_ignore(self):
        instr = self.parse('int f(void) {} int main(void) { int a; f(); a = f();}')

        self.assert_contains_sequence(instr, [
            (CALL, "f", 0, None),
            (POP, None, None, None),
            (CALL, "f", 0, None),
            (ASSIGN, 1, INT, None)
        ])

    def test_builtin1(self):
        instr = self.parse('int main(void) { print(1, 2); }')

        self.assert_contains_sequence(instr, [
            (LOAD_INT, 1, None, None),
            (LOAD_INT, 2, None, None),
            (PRINT_INT, 1, None, None),
            (PRINT_INT, 0, None, None),
            (DECREASE_STACK, 2, None, None)
        ])

    def parse(self, input):
        parser = Parser(debug=1)
        return parser.parse(input)

    def assert_contains_sequence(self, list, sublist):
        for item in list:
            if len(sublist) > 0 and item == sublist[0]:
                sublist = sublist[1:]

        if (len(sublist) > 0):
            print sublist
        self.assertTrue(len(sublist) == 0)

    # def tearDown(self):

if __name__ == '__main__':
    unittest.main(verbosity=2)
